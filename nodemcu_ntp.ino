#include <NTPClient.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

const char *ssid     = "DVRTESTE";
const char *password = "v2techwifi";

WiFiUDP ntpUDP;
int16_t utc = -2;
NTPClient timeClient(ntpUDP, "a.st1.ntp.br", utc*3600, 60000);

time_t now;
struct ts;
char buf[80];

void setup(){
  Serial.begin(115200);
  WiFi.begin(ssid, password);

  while ( WiFi.status() != WL_CONNECTED ) {
    delay ( 500 );
    Serial.print ( "." );
  }

  timeClient.begin();
}

void loop() {
  timeClient.update();
  Serial.println(timeClient.getFormattedTime());
  Serial.println(timeClient.getEpochTime());
  Serial.println("-----------------------");
  delay(10000);
}
